from .activation import (  # noqa
    ActivationEmailsView, ResetPassword, SendActivationEmailView
)
from .contact_info import (  # noqa
    AddContact, DeleteContact, ListContacts, UpdateContact, UpdateContactBase,
    UpdatePersonalInfo, get_permission
)
