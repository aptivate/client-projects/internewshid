from django import forms

import transport
from hid.constants import ITEM_TYPE_CATEGORY


class AddEditItemForm(forms.Form):
    id = forms.CharField(
        widget=forms.HiddenInput,
        required=True
    )
    body = forms.CharField(
        widget=forms.Textarea,
        required=True
    )
    translation = forms.CharField(
        widget=forms.Textarea,
        required=False
    )
    location = forms.CharField(
        widget=forms.TextInput,
        required=False
    )
    sub_location = forms.CharField(
        widget=forms.TextInput,
        required=False
    )
    language = forms.CharField(
        widget=forms.TextInput,
        required=False
    )
    risk = forms.CharField(
        widget=forms.TextInput,
        required=False
    )
    gender = forms.CharField(
        widget=forms.TextInput,
        required=False
    )
    contributor = forms.CharField(
        widget=forms.TextInput,
        required=False
    )
    collection_type = forms.CharField(
        widget=forms.TextInput,
        required=False
    )
    timestamp = forms.DateTimeField(required=True)
    next = forms.CharField(
        widget=forms.HiddenInput,
        required=True
    )
    tags = forms.CharField(
        widget=forms.TextInput,
        required=False
    )

    def __init__(self, *args, **kwargs):
        feedback_disabled = kwargs.pop('feedback_disabled', False)
        form_disabled = kwargs.pop('form_disabled', False)
        keyvalues = kwargs.pop('keyvalues', False)

        super(AddEditItemForm, self).__init__(*args, **kwargs)

        if form_disabled:
            for key in self.fields:
                if not isinstance(self.fields[key].widget, forms.widgets.HiddenInput):
                    self.fields[key].disabled = True

        self.fields['body'].disabled = feedback_disabled

        self._maybe_add_category_field()
        self._maybe_add_feedback_type_field()
        self._maybe_add_age_range_field()

        if keyvalues:
            self._add_key_value_fields(keyvalues)

    def _maybe_add_category_field(self):
        # This used to be more flexible in that we had partial
        # support for per- item/feedback/message type categories
        # but it was never fully implemented and it was confusing
        # so for now we have one set of categories for all types
        choices = self._get_category_choices()

        if choices is not None:
            self.fields['category'] = forms.ChoiceField(
                choices=choices, required=False
            )

    def _get_category_choices(self):
        return self._get_term_choices(ITEM_TYPE_CATEGORY['all'], createBlank=True)

    def _maybe_add_feedback_type_field(self):
        choices = self._get_feedback_type_choices()

        if choices is not None:
            self.fields['feedback_type'] = forms.MultipleChoiceField(
                choices=choices,
                required=False
            )

    def _get_feedback_type_choices(self):
        return self._get_term_choices('item-types', createBlank=False)

    def _maybe_add_age_range_field(self):
        choices = self._get_age_range_choices()

        if choices is not None:
            self.fields['age_range'] = forms.ChoiceField(
                choices=choices,
                required=False,
                widget=forms.RadioSelect()
            )

    def _get_age_range_choices(self):
        return self._get_term_choices('age-ranges', createBlank=True)

    def _get_term_choices(self, taxonomy, createBlank):
        terms = transport.terms.list(
            taxonomy=taxonomy
        )

        if len(terms) > 0:
            sorted_terms = sorted(terms, key=lambda k: k['name'].lower())

            if createBlank:
                choices = (('', '-----'),)
            else:
                choices = ()

            # TODO: We should use 'name' and 'long_name' so that
            # internationalisation is possible but we need to account for
            # where there is no 'long_name' and should fall back on 'name'
            # see also view_and_edit_table.py

            # use a capitalised version of name if long_name is missing
            for t in sorted_terms:
                choice_text = t.get('long_name', t['name'].title())

                # If there is a blank string in long_name then use the short name.
                if not choice_text:
                    choice_text = t['name'].title()

                choices += ((t['name'], choice_text),)

            return choices

        return None

    def _add_key_value_fields(self, keyvalues):
        for key, value in keyvalues.items():
            field_name = f'item-keyvalue-{key}'
            self.fields[field_name] = forms.CharField(
                required=False, label=key
            )

    def get_key_value_fields(self):
        for field_name in self.fields:
            if field_name.startswith('item-keyvalue-'):
                yield self[field_name]
