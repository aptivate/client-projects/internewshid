from django.contrib import messages
from django.http import HttpResponseRedirect
from django.utils.translation import gettext, ungettext
from django.views.generic import FormView

from chn_spreadsheet.importer import Importer, SheetImportException

from ..forms.upload import UploadForm


class UploadSpreadsheetView(FormView):
    form_class = UploadForm
    template_name = "hid/upload.html"

    def get_success_url(self):
        return self.request.POST.get('next')

    def form_valid(self, form):
        data = form.cleaned_data
        source = data['source']
        uploaded_file = data['file']

        form_enabled = False
        if hasattr(self.request, 'user'):
            if self.request.user.has_perm('data_layer.change_message'):
                form_enabled = True

        if form_enabled:
            try:
                importer = self.get_importer()
                (saved, skipped) = importer.store_spreadsheet(
                    source, uploaded_file
                )
                all_messages = [
                    gettext("Upload successful!"),
                    ungettext("{0} entry has been added.",
                              "{0} entries have been added.",
                              saved).format(saved)
                ]

                if skipped > 0:
                    all_messages.append(
                        ungettext("{0} duplicate entry was skipped.",
                                  "{0} duplicate entries were skipped.",
                                  skipped).format(skipped)
                    )

                messages.success(self.request, ' '.join(all_messages))
            except SheetImportException as exc:
                messages.error(self.request, str(exc))

        return HttpResponseRedirect(self.get_success_url())

    def get_importer(self):
        # allows us to provide a mock implementation in testing
        return Importer()
