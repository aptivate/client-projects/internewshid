import pytest
from rest_framework import status
from rest_framework.test import APIRequestFactory

from ..views import ItemViewSet
from .item_create_view_tests import create_item


def get_item(id):
    view = ItemViewSet.as_view(actions={'get': 'retrieve'})
    request = APIRequestFactory().get('')

    return view(request, pk=id)


@pytest.mark.django_db
def test_keyvalue_created():
    item = create_item(body='Text').data

    kwargs = {
        'key': 'CONTRIBUTER',
        'value': 'TWB',
    }

    request = APIRequestFactory().post('', kwargs)
    view = ItemViewSet.as_view(actions={'post': 'add_keyvalue'})
    response = view(request, item_pk=item['id'])
    assert status.is_success(response.status_code), response.data

    updated_item = get_item(item['id']).data

    values = updated_item['values']

    assert values['CONTRIBUTER'] == 'TWB'


@pytest.mark.django_db
def test_add_keyvalue_raises_if_item_does_not_exist():
    kwargs = {
        'key': 'CONTRIBUTER',
        'value': 'TWB',
    }

    request = APIRequestFactory().post('', kwargs)
    view = ItemViewSet.as_view(actions={'post': 'add_keyvalue'})

    unknown_item_id = 9999
    response = view(request, item_pk=unknown_item_id)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert response.data['detail'] == "Message matching query does not exist."


@pytest.mark.django_db
def test_keyvalue_updated():
    item = create_item(body='Text').data

    kwargs = {
        'key': 'CONTRIBUTER',
        'value': 'TWB',
    }

    request = APIRequestFactory().post('', kwargs)
    view = ItemViewSet.as_view(actions={'post': 'add_keyvalue'})
    response = view(request, item_pk=item['id'])
    assert status.is_success(response.status_code), response.data

    kwargs = {
        'key': 'CONTRIBUTER',
        'value': 'TWB modified',
    }
    request = APIRequestFactory().post('', kwargs)
    view = ItemViewSet.as_view(actions={'post': 'update_keyvalue'})
    response = view(request, item_pk=item['id'])
    assert status.is_success(response.status_code), response.data

    updated_item = get_item(item['id']).data

    values = updated_item['values']

    assert values['CONTRIBUTER'] == 'TWB modified'


@pytest.mark.django_db
def test_update_keyvalue_raises_if_item_does_not_exist():
    kwargs = {
        'key': 'CONTRIBUTER',
        'value': 'TWB',
    }

    request = APIRequestFactory().post('', kwargs)
    view = ItemViewSet.as_view(actions={'post': 'update_keyvalue'})

    unknown_item_id = 9999
    response = view(request, item_pk=unknown_item_id)
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert response.data['detail'] == "Message matching query does not exist."
