import pytest

from data_layer.tests.factories import ItemFactory
from transport.items import list_options


@pytest.mark.django_db
def test_list_options_for_gender_unique():
    ItemFactory(gender='male')
    ItemFactory(gender='male')
    ItemFactory(gender='female')
    ItemFactory(gender='female')
    ItemFactory(gender='other')

    genders = list(list_options('gender'))
    assert genders == ['female', 'male', 'other']


@pytest.mark.django_db
def test_list_options_for_gender_exclude_blank():
    ItemFactory(gender='')
    ItemFactory(gender='female')
    ItemFactory(gender='xie')

    genders = list(list_options('gender'))
    assert '' not in genders


@pytest.mark.django_db
def test_list_options_for_location_unique():
    ItemFactory(location='Cambridge')
    ItemFactory(location='Cambridge')
    ItemFactory(location='Brighton')
    ItemFactory(location='Brighton')
    ItemFactory(location='London')

    actual_locations = list(list_options('location'))
    assert actual_locations == ['Brighton', 'Cambridge', 'London']
