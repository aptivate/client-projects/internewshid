import pytest

from transport import items

from ..exceptions import TransportException


@pytest.fixture
def item_data():
    item = {'body': "What is the cuse of ebola?"}
    return items.create(item)


@pytest.mark.django_db
def test_keyvalues_can_be_added_to_item(item_data):
    item_id = item_data['id']
    response = items.add_keyvalue(item_id, 'CONTRIBUTER', 'TWB')

    assert response['values'] == {'CONTRIBUTER': 'TWB'}


@pytest.mark.django_db
def test_keyvalues_can_be_updated_on_item(item_data):
    item_id = item_data['id']
    response = items.add_keyvalue(item_id, 'CONTRIBUTER', 'TWB')
    response = items.update_keyvalue(item_id, 'CONTRIBUTER', 'TWB modified')

    assert response['values'] == {'CONTRIBUTER': 'TWB modified'}


@pytest.mark.django_db
def test_add_raises_when_item_does_not_exist():
    unknown_item_id = 9999
    with pytest.raises(TransportException) as excinfo:
        items.add_keyvalue(unknown_item_id, 'CONTRIBUTER', 'TWB modified')

        assert excinfo.value.message['status_code'] == 404
        assert excinfo.value.message['value'] == {
            'CONTRIBUTER': 'TWB modified'
        }
        assert excinfo.value.message['item_id'] == 9999


@pytest.mark.django_db
def test_update_raises_when_item_does_not_exist():
    unknown_item_id = 9999
    with pytest.raises(TransportException) as excinfo:
        items.update_keyvalue(unknown_item_id, 'CONTRIBUTER', 'TWB modified')

        assert excinfo.value.message['status_code'] == 404
        assert excinfo.value.message['value'] == {
            'CONTRIBUTER': 'TWB modified'
        }
        assert excinfo.value.message['item_id'] == 9999
