# Internews Humanitarian Information Dashboard

Master:
  * [![pipeline status](https://git.coop/aptivate/internewshid/badges/master/pipeline.svg)](https://git.coop/aptivate/internewshid/commits/master)
  * [![coverage report](https://git.coop/aptivate/internewshid/badges/master/coverage.svg)](https://git.coop/aptivate/internewshid/commits/master)

Staging:
  * [![pipeline status](https://git.coop/aptivate/internewshid/badges/staging/pipeline.svg)](https://git.coop/aptivate/internewshid/commits/staging)
  * [![coverage report](https://git.coop/aptivate/internewshid/badges/staging/coverage.svg)](https://git.coop/aptivate/internewshid/commits/staging)
