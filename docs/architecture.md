# Humanitarian Information Dashboard Architecture

## Overview

The Internews Humanitarian Information Dashboard (HID) allow spreadsheets of message (eg. text messages, rumours etc) to be imported, edited, searched and visualised.

It has a flexible database structure that allows arbitrary tagging and data to be imported with the message.

## General Architecture

The Internews HID application is written in Django.

### API
It uses the **DjangoREST framework** to provide an API directly to the data. All of the internal data access is done through the API rather than directly to the database. See the `transport` app.

### Flexible Database
The application has a number of features that are configured or stored in the database rather than coded in the structure which in principle allows them to be modified by an admin user in the django admin user interface.

For instance the specification of spreadsheet data formats used by the spreadsheet importer can be specified in the admin interface.

Also the data itself uses two mechanisms to allow flexibility in data storage. It has the concept of Tags and KeyValue pairs.

The tags belong to taxonomies that are specified through the admin interface. A taxonomy can be closed (in which case all values that a tag of that taxonomy can take must be specified) or they can be open in which case the importer will create a new tag in the database if it comes across a new entry in the imported data. This means that taxonomies of tags that can be attached to message records are configurable and not hard-coded in the database.

Similarly there are KeyValue pairs. Instead of importing data into a set database column, they allow and administrator to configure "virtual" columns by specifying a spreadsheet column as a KeyValue type. Additionally the importer can import any  spreadsheet columns that have not been specified in the importer specification into KeyValue pairs.   

## CSS / LESS

The CSS is auto-generated from .less files.
There should be no need to manually run compass.

### InterNews HID theme

LESS files reside in internewshid/internewshid/media/less

### Bootstrap 3.3.5

We are using Bootstrap v3.3.5 (http://getbootstrap.com) as our base styling.
See /media/bootstrap/less/bootstrap.less for the configuration, not all files are imported.
We do not load glyph-icons for example.

### Dashboard

Has its own css file dashboard/dashboard.css, loaded in assets.py

### Fontawesome icons

Fontawesome 4.3.0 is loaded by assets.py
Source files: /mediafonts/font-awesome-4.3.0/


## Adding a new spreadsheet format

Create a new `SheetProfile` object with a `profile`, for example:
```
    {
        "name":"Geopoll",
        "format":"excel",
        "label":"geopoll",
        "skip_header":1,
        "taxonomies":{
            "contexts":"Ebola-Liberia",
            "countries":"Liberia",
            "data-origins":"Geopoll Spreadsheet",
            "item-types":"question"
        },
        "columns":[
            {
                "field":"terms",
                "type":"taxonomy",
                "name":"Province",
                "taxonomy":"tags"
            },
            {
                "field":"timestamp",
                "type":"date",
                "name":"CreatedDate",
                "date_format":"%m/%d/%y"
            },
            {
                "field":"ignore",
                "type":"ignore",
                "name":"AgeGroup"
            },
            {
                "field":"body",
                "type":"text",
                "name":"QuestIO"
            }
        ]
  }
```

`name`
: A human-readable name for this profile

`format`
: File format (currently must be `excel`)

`label`
: Unique identifier (not currently used???)

`skip_header`
: If `0` or absent use the profile's order of columns. Otherwise use the header to define the order

`columns`
: An array of columns to be imported from the spreadsheet. See [Column keys](#column-keys)

### Column keys

`field`
: See [Fields](#fields)

`type`
: Supported types are `date`, `text`, `integer`, `number` (decimal), `taxonomy`. This can also be set to `ignore` for ignored fields.

`name`
: The column heading in the spreadsheet

`date_format`
: String used to import the cells of `date` type (as used by [strptime](https://docs.python.org/2/library/datetime.html#strftime-strptime-behavior))

`taxonomy`
: Taxonomy to use when importing terms

### Fields

`ignore`
: This column is not imported

`body`
: The main body of the message. Stored as `body` on the `Item` object

`timestamp`
: Date when reported. Stored as `timestamp` on the `Item` object

`terms`
: Import field as taxonomy terms (eg tags)

## Creating a new Tabbed Page

A tabbed page has a `name` and one or more tab instances. These determine the pages of the View & Edit screen.

The `Settings` are specified in a blob of JSON with the following format:
```
{
  "collection_type":"kobo",
  "columns":[
    "select_item",
    "created",
    "timestamp",
    "body",
    "category"
  ],
  "categories":[
    "ebola-questions"
  ],
  "filters":{
    "terms":[
      "item-types:question"
    ]
  },
  "label":"Questions"
}
```

`collection_type`
: Matches the `SheetProfile` `label` above

`columns`
: Which columns to display in the table

`categories`
: ???

`filters`
: Which filters to apply when listing items in this tab

`label`
: ???

## Importing a spreadsheet

Importing a spreadsheet will create ``Item`` objects (see `data_layer/models.py` via the ``transport`` app).

## Next steps for future development

The spreadsheet importer was originally developed to handle the export of form based survey tools like KOBO (eg. for the Ebola crisis in Liberia and Bangladesh). Consequently it is deliberately unforgiving of any deviations in the specified import format.

Later it was configured for use in DRC, South Sudan and for the COVID19 crisis, where data had been manually entered into spreadsheets. The importer could be improved for this scenario by being more tolerant. For instance it could force standard capitalisation and strip whitespace. It could also better report importing errors and allow for correction and re-importing workflow.

The importer specification was designed to be flexible without having to change the code so it is set in the admin interface. It is exposed in the admin as an editable JSON structure. This isn't as user friendly as it could be and also it complicates the code significantly. The importer specification is a relatively technical bit of configuration and was not configured by admin users. Instead it was developers who edited it. It would be worth reviewing whether editing the importer specification in the admin interface is the best option or if editing it as source code would be better.
