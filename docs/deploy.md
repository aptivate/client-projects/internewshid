# How to set up the Internews Humanitarian Information Dashboard on a Ubuntu 18.04 environment

This document provides instructions on deploying the [Internews Humanitation
Information Dashboard](https://git.coop/aptivate/client-projects/internewshid/) on a [Ubuntu
18.04 LTS](http://releases.ubuntu.com/18.04/) environment.

Familiarity with the command line is required.

## Development environment setup

These instructions allow you to setup the HID on a local development machine,
based on the desktop edition of Ubuntu 18.04. This setup does not require a web
server, as it is for development only and instead uses Django's build in
server.

All instructions are to be run in a
[terminal](https://help.ubuntu.com/community/UsingTheTerminal), and require a
user who can [run sudo](https://help.ubuntu.com/community/RootSudo)

### Pre-requisites

You need to install a MySql server, git, a Python development environment and
other system tools:

```sh
    sudo apt-get update
    sudo apt-get install -y default-libmysqlclient-dev python-pymysql python-mysqldb nodejs node-less
```

You will need to know your MySql root password. If you set it up for the first
time you will be prompted for the password. If you have set it up previously
and forgotten the password you will need to [reset your MySql root
password](https://help.ubuntu.com/community/MysqlPasswordReset).

### Fetch and prepare Internews HID application

We will store the internews hid in the user's home directory, under
`~/projects/internewshid`. Feel free to place it anywhere else.

First, get a copy of internews hid:

```sh
    mkdir -p ~/projects
    cd ~/projects
    git clone git@git.coop:aptivate/client-projects/internewshid.git
```

This will prompt you for your Github user account. Next you will want to
download the application's dependencies and deploy the project locally:

```sh
    cd internewshid
    ln -srf internewshid/local_settings.py.dev internewshid/local_settings.py
    echo "SECRET_KEY = '$DJANGO_SECRET_KEY'" >> internewshid/private_settings.py
    echo "DB_PASSWORD = 'internewshid'" >> internewshid/private_settings.py
    sudo mysql -ve "CREATE DATABASE IF NOT EXISTS internewshid CHARACTER SET utf8 COLLATE utf8_general_ci;"
    sudo mysql -ve "CREATE USER 'internewshid'@'localhost' IDENTIFIED BY 'internewshid'"
    sudo mysql -ve "GRANT ALL ON internewshid.* TO 'internewshid'@'localhost'; FLUSH PRIVILEGES;"
    pip install pipenv && pipenv sync --dev
```

Note there are a number of different settings files (eg. `local_settings.py.covid19-dev`) and you will want to set your local settings to the appropriate settings file using a symbolic link, depending on what you are doing. For instance if you are doing development on the COVID19 version on your local machine you want to use `local_settings.py.covid19-dev`. The generic development settings are `local_settings.py.dev` which is what the following command uses (from the set of commands above). You can change this command to the appropriate file.

```sh
    ln -srf internewshid/local_settings.py.dev internewshid/local_settings.py
```

When you deploy to your production server you will want to use the appropriate production settings file. 


### Load the database fixtures

The dashboard is configured through the database. This is most easily set up through
fixture files.
There are many fixture files depending on the context, for example:

```sh
    pipenv run python manage.py loaddata covid-19
```

The final preparation step is to setup a super user who will have access to the administration interface:

```sh
    pipenv run python manage.py createsuperuser
```

### Run the tests

To ensure long term maintainability the application contains a number of
automated tests. If you want to run the tests you will need to install
additional dependencies:

```sh
    sudo apt-get install -y phantomjs
```

The automated tests can then be executing by running:

```sh
    pipenv run pytest
```

### Run the Internews HID application

You can start Django's internal web server by running the following command:

```sh
    pipenv run python manage.py runserver
```

Once this is started, you can point your web browser to `http://localhost:8000`
to see the Internews HID.

### Notes on the development version

In the development version, the javascript and CSS assets are not compressed,
and not combined into a single file. This makes development easier - however it
means the page size will be considerably larger than it would be in production.
